#include <iostream>
#include <string>

struct Fraction{
    Fraction( int n = 0, int m = 1 ) : numer(n), denom(m) { }

    int numer;
    int denom;
};

bool operator<( const Fraction& lhs, const Fraction& rhs ){
    int p = lhs.numer;
    int q = lhs.denom;
    int r = rhs.numer;
    int s = rhs.denom;
    return p*s < r*q;
}


namespace Pic10b {
    // WANT A VERSION THAT WORKS FOR ANY TYPE FOR WHOM THIS CODE MAKES SENSE
    //
    // This can be done with templates. Like this:
    template <typename ItemType>
    bool min( const ItemType& a, const ItemType& b ){
        if ( a < b )
            return true;
        else 
            return false;
    }
    // However, we inadvertently, changed the name of the function.
    // It is no longer just `Pic10b::min`.
    // It is now
    //     Pic10b::min<ItemType>
    //
    // We can still use the _old_ name (see main).


    // We can still have specialized versions!
    bool min( const Fraction& a, const Fraction& b ){
        std::cout << "Special version: arguments interpreted as Fractions.\n";
        if ( a < b )
            return true;
        return false;
    }
}


int main(){
    using namespace Pic10b;   // `min` means `Pic10b::min`

    int p = 0;
    int q = 1;
    if ( min(p,q) )
        std::cout << "0 is less than 1.\n";

    double x = 0.0;
    double y = 1.1;
    if ( min(x,y) )
        std::cout << "0.0 is less than 1.1\n";

    x = 1.0;
    y = 1.1;
    if ( min(x,y) )
        std::cout << "1.0 is less than 1.1\n";

    std::string r = "Erre";  // <-- 'R' in Spanish
    std::string s = "Ese";   // <-- 'S' in Spanish
    if ( Pic10b::min(r,s) )
        std::cout << "R is less than S\n";


    Fraction half(1,2);
    Fraction one(1);
    if ( min(half,one) )
        std::cout << "half is less than one\n";

    if ( min(p,one) )  // Can mix & match (as long as conversions make sense)
        std::cout << "0 (int) is less than 1 (fraction).\n";

    if ( min(p,x) )    // Do not match, but can be made to match via conversion
        std::cout << "0 (int) is less than 1 (double).\n";

    if ( min<double>(p,x) ) // Or we can specify wich version we want
        std::cout << "0 (int) is less than 1 (double). Compared via double.\n";

    Fraction two(2.0);    // Indeed. Doubles are fractions via int ctor.
    Fraction two_point_one(2.1); // Indeed. Doubles are fractions via int ctor.

    return 0;
}